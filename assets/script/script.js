// Variables: 
// Data Storage

// To declare a variable, we'lluse the keywords let or const:
// We will create the cariable for the first time.

let student = "Brandon";
// console.log(student);

const day = "Monday"; 
// console.log(day);

// const is short for constant
// We cannot reassign a value
// day ="Tuesday"
// console.log(day);

student = "Brenda";
// console.log(student);

student = 3.1416;
// console.log(student);

// Data Types:
// 1. string - denoted by quotation marks;
// 	- series of characters
// 	Ex: "Brandon", "1234", "String Sample"
// 	Series of characters

// 2. number - can be used in MAthematical operations.
 // Ex. 2, 3.15, 0.25, 450
// 3. boolean - true or fals

// To check the data type
console.log(typeof "Am I a string?") //string
console.log(typeof "1234"); //string
console.log(typeof 1234); //number
console.log(typeof "true"); //string
console.log(typeof false); //boolean
console.log(typeof whatami); //error

// Operators

// Arithmetic Operators
// +, -, *, /,
const num1 = 10;
const num2 = 2;
const num3 = 6;

console.log(num1 + num2); //12

//% - modulo --> remainder

//Assignment Operators;
// = --> we are assigning a value to a variable;

// right side variable on left side

// Assignment - Arithmetic;
// +=, -=, *=, /=, %=

let output = 0;
const num4 = 5;
const num5 = 8;


output = output + num4; //5
output += num4; //add num4 to the previous
// value of output

// - = subtract num4 from the prvious value;


