/**
 * Give at least three reasons why you should learn javascript.
 */

1. Better user experience for faster, interactive responsive pages
2. Easy to learn and easy to implement
3. Javascript is EVERYWHERE!

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. Javascript is used to make pages more interactive
2. Javascript allows us to dynamically change the content from inside the browser without reloading the page

/**
 * What is a variable? Give at least three examples.
 */

1. let and const
Examples:
	1. let girl = "Tammy";
	2. let boy = "Mike";
	3. const month = "June";

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> String, Number, Boolean

String:
"hello", "1234"

Numbr:
1234, 0.5

Boolean:
true, false


/**
 * Write the correct data type of the following:
 */

1. "Number" - string
2. 158 - number
3. pi - error
4. false - boolean
5. 'true' - string
6. 'Not a String' -  string

/**
 * What are the arithmetic operators?
 */ +=, -=, *=, /=

/**
 * What is a modulo? 
 */ Modulo represented by the symbol (%) means showing the remainder of the equation.

/**
 * Interpret the following code
 */
------------------------------------------
let number = 4;
number += 4;

Q: What is the value of number?

8
------------------------------------------
let number = 4;
number -= 4;

Q: What is the value of number?

0
------------------------------------------
let number = 4;
number *= 4;

Q: What is the value of number?

16
------------------------------------------
let number = 4;
number /= 4;

Q: What is the value of number?

1
------------------------------------------